#include "steer_processor.h"

void steer_processor(uint32_t left_sensor_cm, uint32_t right_sensor_cm) {
  uint32_t steering_threshold = 50;
  if (steering_threshold > left_sensor_cm || steering_threshold > right_sensor_cm) {
    if (right_sensor_cm > left_sensor_cm) {
      steer_right();
    } else if (left_sensor_cm > right_sensor_cm) {
      steer_left();
    }
  }
}