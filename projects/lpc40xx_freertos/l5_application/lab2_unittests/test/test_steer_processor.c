#include "Mocksteering.h"
#include "steer_processor.h"
#include "unity.h"

void test_steer_processor__move_left(void) {
  steer_left_Expect();
  steer_processor(75, 49);
}

void test_steer_processor__move_right(void) {
  steer_right_Expect();
  steer_processor(49, 75);
}

void test_steer_processor__both_sensors_less_than_threshold(void) {
  // case1: If both sensors are of equal value and the value less than the threshold, no steering should occur.
  steer_processor(49, 49);

  // Steer right if both sensors are less than the threshold but left is bigger than the right.
  steer_right_Expect();
  steer_processor(25, 49);

  // Steer left if both sensors are less than the threshold but right is bigger than the left.
  steer_left_Expect();
  steer_processor(45, 25);
}

// Hint: If you do not setup an Expect()
// then this test will only pass none of the steer functions is called
void test_steer_processor__both_sensors_more_than_threshold(void) {
  // If both sensors are of equal value and above the threshold no steering should occur.
  steer_processor(75, 75);

  // Even if both sensors are above the threshold and left is larger, no steering should occur.
  steer_processor(75, 50);

  // Even if both sensors are above the threshold and right is larger, no steering should occur.
  steer_processor(50, 75);
}

// Do not modify this test case
// Modify your implementation of steer_processor() to make it pass
// This tests corner case of both sensors below the threshold
void test_steer_processor(void) {
  steer_right_Expect();
  steer_processor(10, 20);

  steer_left_Expect();
  steer_processor(20, 10);
}