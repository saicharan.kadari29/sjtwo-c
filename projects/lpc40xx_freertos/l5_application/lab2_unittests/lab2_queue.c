#include "lab2_queue.h"
#include <string.h>

void queue__init(queue_s *queue) {
  // Initialize the entire structure to zero 
  if (NULL != queue) {
    memset(queue, 0, sizeof(queue_s));
    queue->size = 100; // Matches the size of queue_memory.
  }
}

bool queue__push(queue_s *queue, uint8_t push_value) {
  bool status = false;

  if (NULL != queue) {
    if (queue->used < queue->size) {
      // Assuming that the current pushed_value is at index 0 or higher, push the value to the first element in the
      // queue.
      queue->queue_memory[queue->pushed_index] = push_value;

      // Incremementing the pushed index to point, to the next element to add.
      queue->pushed_index++;

      // Incremementing the usage value up, until queue is full.
      queue->used++;

        if (queue->pushed_index >= queue->size) {
        queue->pushed_index = 0;
      }
      status = true;
    }
  }
  return status;
}

bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  bool status = false;

  if (NULL != queue && NULL != pop_value && queue->used > 0) {
    // Set the value at the address pointed to by pop_value to the current value at the end of the FIFO queue.
    *pop_value = queue->queue_memory[queue->popped_index];

    // Incrememt the pointer to point to the next element to pop.
    queue->popped_index++;
    queue->used--;

    // as in with push(), Keep adding elements.
    if (queue->popped_index >= queue->size) {
      queue->popped_index = 0;
    }
    status = true;
  }
  return status;
}

size_t queue__get_item_count(const queue_s *queue) {
  // The item count is determined based on the current value of the pushed_index.
  return (size_t)queue->used;
}